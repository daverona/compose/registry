# daverona/compose/registry

## Prerequisites

* `registry.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/compose/traefik) out

## Quick Start

```bash
cp .env.example .env
# edit .env
cp storage/registry/auth/htpasswd.example storage/registry/auth/htpasswd
docker-compose up --detach
```

> `storage/registry/auth/htpasswd` has a user `admin` with password `secret` by default.
> You must remove this account and add yours. To do so, read Usage section.

Let's test the new registry setup. We simply login to the registry,
push an image, and pull the image back and logout:

```bash
# get hello-world image
docker image pull hello-world
# prepare to push the image to registry "registry.example"
docker image tag hello-world registry.example/hello-world
# sign in to registry.example
docker login --username admin --password secret registry.example
# push the image to registry.example
docker image push registry.example/hello-world
# remove the local images
docker image rm hello-world registry.example/hello-world
# pull the image from registry.example
docker image pull registry.example/hello-world
# list the local images
docker image ls | grep hello-world
```

## Usage

### Add User

```bash
docker container run --rm -it httpd:alpine htpasswd -n myname
# enter password
```

Append the output to `storage/regisitry/auth/htpasswd` file.

### Use Docker Registry HTTP APIs

#### List Images

```bash
curl --location --insecure --user admin:secret https://registry.example/v2/_catalog
```

#### List Image Tags

```bash
curl --location --insecure --user admin:secret https://registry.example/v2/hello-world/tags/list
```

#### Delete Image

```bash
# get the digest, which prints after "docker-content-digest:"
curl --location --insecure --user admin:secret \
  --header "Accept: application/vnd.docker.distribution.manifest.v2+json" \
  --head --request GET https://registry.example/v2/hello-world/manifests/latest
# delete the image
curl --location --insecure --user admin:secret \
  --header "Accept: application/vnd.docker.distribution.manifest.v2+json" \
  --head --request DELETE https://registry.example/v2/hello-world/manifests/sha256:...
```

#### Registry Maintenance

If you have a lot of images deleted in a short period of time, manual maintenance may be required.
Registry container should start *read-only* mode, collect garbage, and restart in *read-write* mode:

```bash
# set REGISTRY_STORAGE_MAINTENANCE_READONLY_ENABLED to true in .env.
docker-compose up --detach
docker-compose exec registry bin/registry garbage-collect /etc/docker/registry/config.yml 
# set REGISTRY_STORAGE_MAINTENANCE_READONLY_ENABLED to false in .env.
docker-compose up --detach
```

For more information, please read these articles:

* [https://docs.docker.com/registry/configuration/#readonly](https://docs.docker.com/registry/configuration/#readonly)
* [https://docs.docker.com/registry/garbage-collection/#run-garbage-collection](https://docs.docker.com/registry/garbage-collection/#run-garbage-collection)

> Registry container periodically removes orphaned files from the upload directory with `uploadpurging` options.
> Now I wonder if images without manifests are orphant files.

## References

* Deploy a registry server: [https://docs.docker.com/registry/deploying/](https://docs.docker.com/registry/deploying/)
* Authenticate proxy with nginx: [https://docs.docker.com/registry/recipes/nginx/](https://docs.docker.com/registry/recipes/nginx/)
* Configuring a registry: [https://docs.docker.com/registry/configuration/](https://docs.docker.com/registry/configuration/)
* Docker Registry HTTP API V2: [https://docs.docker.com/registry/spec/api/](https://docs.docker.com/registry/spec/api/)
